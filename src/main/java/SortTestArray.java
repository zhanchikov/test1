import java.io.*;
import java.util.*;

public class SortTestArray {

    public void sortIntArrayReverseOrder() throws IOException {

        File f = new File("C:/Users/tim/IdeaProjects/ab/src/main/resources/num.txt");
        Scanner s = new Scanner(f).useDelimiter(",");
        ArrayList<Integer> myList = new ArrayList<Integer>();
        while (s.hasNext()) {
            if (s.hasNextInt()) {
                myList.add(s.nextInt());
            } else {
                s.next();
            }
        }

        System.out.print("Сортировка по возрастанию: ");
        Collections.sort(myList);
        for (Integer i : myList) {
            System.out.print(i.intValue() + " ");
        }

        System.out.print("\nСортировка по убыванию: ");
        Collections.sort(myList ,Collections.reverseOrder());
        for (Integer i : myList) {
            System.out.print(i.intValue() + " ");
        }

    }


}

